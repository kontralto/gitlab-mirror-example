#!/bin/bash
# Display the public key for CodeCommit in a given region (can easily be modified for other providers
REGION=us-west-2
HVCS_URL=git-codecommit.$REGION.amazonaws.com
TEMP_KEY_NAME=$HVCS_URL-key-temp
ssh-keyscan -t rsa $HVCS_URL | tee $TEMP_KEY_NAME | ssh-keygen -lf -
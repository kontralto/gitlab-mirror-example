#!/bin/bash
### Creates a git remote named mirror if it doesn't exist, and pushes to it using the specified SSH credentials
set -e

if [[ "${DEBUG}" -eq "true" ]]; then
    set -x
fi

GIT_USERNAME=${INPUT_GIT_USERNAME:-${GIT_USERNAME:-"git"}}
REMOTE=${INPUT_REMOTE:-"$*"}
GIT_SSH_PRIVATE_KEY=${INPUT_GIT_SSH_PRIVATE_KEY}
GIT_SSH_PRIVATE_KEY_PATH=${INPUT_GIT_SSH_PRIVATE_KEY_PATH}
GIT_SSH_PUBLIC_KEY=${INPUT_GIT_SSH_PUBLIC_KEY}
GIT_PUSH_ARGS=${INPUT_GIT_PUSH_ARGS:-"--tags --force --prune"}
GIT_SSH_NO_VERIFY_HOST=${INPUT_GIT_SSH_NO_VERIFY_HOST}
GIT_SSH_KNOWN_HOSTS=${INPUT_GIT_SSH_KNOWN_HOSTS}
GIT_COMMIT_BRANCH=${CI_COMMIT_BRANCH:-$(git branch --show-current)}
PUSH_ALL_REFS=${INPUT_PUSH_ALL_REFS:-"false"}
FULL_GIT_REMOTE=''

function remote_exists {
  git remote show mirror $> /dev/null
}

git config --global credential.username "${GIT_USERNAME}"


if [ "${GIT_SSH_PRIVATE_KEY}" != "" ] || [ "${GIT_SSH_PRIVATE_KEY_PATH}" != "" ]; then
  # we're using SSH, so set the URL accordingly
  FULL_GIT_REMOTE="ssh://${GIT_USERNAME}@${REMOTE}"
  if [[ "${GIT_SSH_PRIVATE_KEY}" != "" ]]; then
    mkdir ~/.ssh
    chmod 700 ~/.ssh
    echo "${GIT_SSH_PRIVATE_KEY}" > ~/.ssh/id_rsa
    if [[ "${GIT_SSH_PUBLIC_KEY}" != "" ]]; then
      echo "${GIT_SSH_PUBLIC_KEY}" > ~/.ssh/id_rsa.pub
      chmod 600 ~/.ssh/id_rsa.pub
    fi
    chmod 600 ~/.ssh/id_rsa
    GIT_SSH_PRIVATE_KEY_PATH=~/.ssh/id_rsa
  fi

  if [[ "${GIT_SSH_KNOWN_HOSTS}" != "" ]]; then
    echo "${GIT_SSH_KNOWN_HOSTS}" > ~/.ssh/known_hosts
    git config --global core.sshCommand "ssh -i ${GIT_SSH_PRIVATE_KEY_PATH} -o IdentitiesOnly=yes -o UserKnownHostsFile=~/.ssh/known_hosts"
  else
    if [[ "${GIT_SSH_NO_VERIFY_HOST}" != "true" ]]; then
      echo "WARNING: no known_hosts set and host verification is enabled (the default)"
      echo "WARNING: this job will fail due to host verification issues"
      echo "Please either provide the GIT_SSH_KNOWN_HOSTS or GIT_SSH_NO_VERIFY_HOST inputs"
      exit 1
    else
      git config --global core.sshCommand "ssh -i ${GIT_SSH_PRIVATE_KEY_PATH} -o IdentitiesOnly=yes -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
    fi
  fi
else
  # we're using HTTPS, so set the URL accordingly
  FULL_GIT_REMOTE="https://${REMOTE}"
  git config --global core.askPass /cred-helper.sh
  git config --global credential.helper cache
fi

if ! remote_exists ; then
  git remote add mirror "${FULL_GIT_REMOTE}"
fi

if [[ "${PUSH_ALL_REFS}" != "false" ]]; then
  eval git push ${GIT_PUSH_ARGS} mirror "\"refs/remotes/origin/*:refs/heads/*\""
else
  eval git push -u ${GIT_PUSH_ARGS} mirror "HEAD:refs/heads/$GIT_COMMIT_BRANCH"
fi
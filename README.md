# GitLab Repository Mirroring Example

This is a sample repo to demonstrate mirroring files to another HVCS (hosted version control system) provider 
(GitHub, CodeCommit, etc). Current functionality is based around SSH as the transport protocol, but HTTPS should work if 
you can pass in the credentials to git without prompting for them (since this isn't possible in the pipeline).

## How it works

The `.gitlab-ci.yml` file has a single pipeline stage, with a single task `codecommit_mirror`, that runs the 
`push_to_mirror` script upon changes to files matching patterns listed under `only: changes`. As this was originally 
meant to mirror source code files that are driving automation (CDK Pipelines) in CodePipeline, we restrict it to files 
relevant for this purpose as well as the CI config file itself so that the pipeline self-mutates.

## Try it yourself

You can try out the push-based mirroring yourself, with this very repo. The following sections will guide you through 
the steps necessary.

### Fork or clone this repo

Having a copy of this codebase is the easiest and fastest way to see it in action. Fork or clone it, create the repo in 
GitLab if the latter, then follow the rest of the steps.

### Disable public CI/CD on your repo (important!!!)

By default, a new GitLab repository will show CI/CD pipeline logs publicly. This is a problem, as the mirroring script 
ends up echoing the SSH private key used (if you can think of a slick way of solving this,
[let us know!](mailto:info@kontralto.com)). The simplest remedy is to go to **Settings > General > Visibility, project 
features, permissions** and change **CI/CD** to "Only Project Members" in the dropdown. Now you can restrict as needed, 
internally within your GitLab group, in order to provide access control to sensitive logs.  

### GitLab CI/CD Variables

The following variables need to be created in the repo's CI/CD settings. Note that you can similarly set them on your 
local system to test the script or manually mirror in case of disaster recovery (`INPUT_GIT_SSH_PRIVATE_KEY_PATH` to 
use a local SSH key, and `INPUT_PUSH_ALL_REFS=true` to push the entire repo and not just the current branch).

* **INPUT_GIT_SSH_PRIVATE_KEY**: This is the SSH private key used to do the push, pasted as the full contents of the 
PEM/key file. Reminder on the format:
`-----BEGIN OPENSSH PRIVATE KEY-----
  key string goes here...
  -----END OPENSSH PRIVATE KEY-----`
* **INPUT_GIT_USERNAME**: The git username to use when pushing, i.e. `AXXXXXXXXXXXXXXXXXXX`
* **INPUT_REMOTE**: URL of the remote to push to, i.e. `git-codecommit.<region>.amazonaws.com/v1/repos/<repo-name>`.
* **INPUT_GIT_SSH_KNOWN_HOSTS** (optional, default: ''): This is the public key of your HVCS provider. 
[A shell script](util/scripts/get_hvcs_public_key.sh) has been provided to retrieve this. If left blank, you will need 
to set `INPUT_GIT_SSH_NO_VERIFY_HOST` or else the push will fail. As an example, here is the value for CodeCommit in 
us-west-2:
    `git-codecommit.us-west-2.amazonaws.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCqyQh/szYdP7ykDdwp8pWMgrFmuXU4mEf27Wzz6QcR/ZJsIRVCdslVjmt9RA1ljanAVPI8zjp/oI6NPICh8l638vRLt8lFbNMfMMfkzxiIXs0mLAuQ2XSbRglPDF4mAQ7C+oD/B01XXKWU4Vack37i0R9RMtkl3spFB8Hq7HJi4/wy87U7FMHfjHWYXr3GAOg1PfRyTAyHij0XXuBhxzz1vMoiI5DkgH8CKuKf+AmW5KHxKX3zPCoKe7nDw3/Re4ghYhc34ldOSZlQin/dIyT/GQy0ayRWGXc0KB2ijrDCxd4N4INBhdqCRDPwkxwqVRUToBFw6Q1zBdlVlK5ViFEV`
* **INPUT_GIT_PUSH_ARGS** (optional, default:`--tags --force --prune`): Args to pass to git, as a string.
* **INPUT_GIT_SSH_NO_VERIFY_HOST** (optional, default: false): Whether to ignore host verification for SSH connections.
* **INPUT_GIT_SSH_PRIVATE_KEY_PATH** (optional, default: blank): Path on disk to the private key, used instead of 
INPUT_GIT_SSH_PRIVATE_KEY for local testing when set.
* **INPUT_PUSH_ALL_REFS** (optional, default: false): Whether to push all refs (i.e. branches) on any commit, or just 
the current ref (branch).

### Create and/or configure the CodeCommit repository

Now you'll need to create a CodeCommit repository if you don't already have one, and then configure it for SSH access. CodeCommit 
requires this be done through an IAM user, so be sure to configure your private key on a user that has the 
necessary permissions to push to CodeCommit. The AWS managed policy `AWSCodeCommitPowerUser` is sufficient for this.

### Push a change or trigger the pipeline manually

You're ready to try out the mirroring - either make a branch and push a change, or trigger the pipeline manually to push 
`main` to your CodeCommit remote.

Let us know if you have any issues! [info@kontralto.com](mailto:info@kontralto.com)